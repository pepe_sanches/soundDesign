ImproTool (python) next steps
# General principles
1. Dependency between pattern and rythm (default, configurable, on-the fly)
2. Implementation of rythm pattern (isobar, some manual patterns with CC codes?)

Define base pattern (as dataset) and play it
Adopt combination o patterns, scales and note leghts (pattern vs decorators)

3. Detect read song bpm and adopt played patterns
4. Pattern can be selected, can be random
5. Add keyboard or midi steering for scales, pattern, leghts
~~6. Add pattern selection depending on interval provided from keyboard - ~~ done
~~7. Later scales can be suggested, selected, randomised, - ~~ done
8. Predefined sets for different kind of music e.g. blues, later jazz, any others types
~~9. Add your scales to scale.py~~ Done slightly different manner 
10. Eventually Gather different backgrounds tracks for standard genres
11. Multi-instrument support
12. Tensor flow/magenta generations? Or maybe tensor selected impro patterns


Problems:
NA 1) How to deal with lengths of notes and tempo, specially when converting to midi events.
This calculation seem to be heart of playing.
Can isobar be used for that?

2) Midi clock support.

OK 3) when using isobar and timeline - how to add additional sounds  - timeline.background() instead of run()


What to do with Rythmn patterns:
~~(Idea: Express patterns with ticks)).~~
~~For start just focus on standard patterns (not necessarily equally distributed,but start them first).~~
 
Skip for a moment Ornamentacje and maybe focus on diminuncje (which are about how to come from one tone to other, but
but have slightly higher resolution as patterns.


Standard patterns do as rule of beat (length of beat)
So, when you have measure 4/4 - there are quarter notes as main note (and beat) and there is 4 of quarter notes in beat.
Phrase can be like 4  bars (16 beats) or (most likely) 8  or 16 bars.

e.g. 3/4 give 3 quarternotes in bar.
e.g. 3/8 give 3 eights
e.g. 5/4 give 5 quarternotes.

Base is 4/4
Important rythms:
* 8x8ths/beat (accents 1,5,3,7)
* triple  (3-trios for 2x8th) => 12 trios/beat (accents 1,7,4,10)
* kwintuplet (5-kwin for 4x8ths)  => 10 kwintuplets (accents 1,6)
* 5/4 accents are 1,4,6
Accents
1234567890
x  x  x x x
1  2  3 4

Accents in 4/4 can be (128-level velocity) 105,80,95,80


"Diminucje" pochody dzwiękowe.
Express as half-beat e.g. 4/4  - only covers 2 quarter notes (could be theoretically decreased)
* 1/2
* 1/4 + 1/8 + 1/8
* 1/8 + 1/8 + 1/4
* 4x 1/8
*  1/8+.  1/16    4x 1/16
*  1/8  2x1/16    4x 1/16
*  8x 1/16

Decision to be taken:
* implement as part of isobar sequences (of velocity and duration - after aligning values) - partially done
NA ~~* or using own structures (but maybe later applying to sequences)  - better to be as int/and fractint .~~

PSequence for time could be confusing since it changes with change of tempo (or resolution).
I want to have possibilitiy to calculate that on the fly.
Or maybe there is psequence easy recalculation....

# Synchronicity
Midi clock has
clock, start, stop, continue (as base).
is missing sync option though.
Check here is to evaluate what are the option of syncing.
* before synchronizing with Ableton (which should be first usable permament target) create kind-of click track that behaves alike Ableton (or just click track) - clock if fine, but also remember about start stop continue ewentually songpose, or even 3rd prio program changes signal
* audible metronome as addon to click track.
 temporary - use midi with sound

Work on easy example first as prototype on how you want to play.


## Scheduling patterns with clock + actually synchronization
timeline.background() allows any timing (this is live).
~~This actually can be achieved by getting one track with click track and second track listening to that.~~  
~~Synchronization by click gives to preserve tempo. How to synch with beats?~~ Has been implemented different way
- scheduling of patterns work, but really this is about synchronization with e.g. ableton now.

~~## create loop with pattern ref replaced by other pattern (using sync) -~~ Done

## check different sources of midi clock
### midi file being played (by mido, but also by diff mid players)
Received MIDI: program_change channel=0 program=0 time=0  = PC message might be used to sync since it goes at start

### midi clock tool
Currently metronome and play along open midi is done.

### ableton (both midi clock version, like "Ableton? sync protocol" or OSC
Ableton midi sync dump
2022-06-09 17:36:04.621986  - Received MIDI: continue time=0
2022-06-09 17:36:10.622986  - Received MIDI: stop time=0
2022-06-09 17:36:10.622986  - Received MIDI: songpos pos=31 time=0
2022-06-09 17:36:10.622986  - Received MIDI: continue time=0
2022-06-09 17:36:11.935008  - Received MIDI: stop time=0
2022-06-09 17:36:12.815977  - Received MIDI: songpos pos=0 time=0
2022-06-09 17:36:12.816971  - Received MIDI: start time=0
2022-06-09 17:36:15.021985  - Received MIDI: stop time=0
2022-06-09 17:36:18.888976  - Received MIDI: songpos pos=0 time=0
2022-06-09 17:36:18.888976  - Received MIDI: start time=0
2022-06-09 17:36:21.466991  - Received MIDI: stop time=0


### by beat-detection python libraries (if any)
### can tick be used for sync (check what happens if isobar tick is used on output where clock being sent)
This is not applicable. Tick is I guess to create one tick in time.

# Tracker
Tracker compose of series of pattern provided as list when scheduling timeline 
- this one is done
## Run tracker along with some ready midi file
I can play midi and play my part.


# User Predefined beats and pattern + apply scale
## Take your patterns, but play using specific scale.
Currently, patterns have ready midi notes.
## Extend isobar with your scales  - done, but as list not as variables
### compare duplicate of scale
### Think about scale up/down version and application in patterns
## Appregios as scales?
## Work with classes always
 Running tracker is different than live, but some bigger tracker appoach can be used as live triggerring (for this is worth to remember about keeping possibility to change tracker while play (shorter like duration, but still synchronized).
Would be good to remember about on-the-fly track changes when submitting tracker (this requires async, replace, but it is possible by action itself, or maybe in action somehow define position counter)  
* Run tracker synchronized with 

# new next steps
## ~~-create general pattern with exact values of steps not intervals. Intervals will be calculated-. -~~ 
Done
## create patter with exact notes values (from scales) - 
Done (draft though)
### ~~play with closes note from selected scale~~ 
This is done automatically - I accept that so far.
### detect scale and assign scale, and play with it (could be dump of all notes for scale is needed (but only withing 0-11 range (denominated/modulated).
### detecting scales can be per beat, bar and can change. Additional condition to select these scales would be genre of music
### Please remember to use different scales for e.g. moll melodyczna up and down
### apart from scale use different keys
### link scales names, but have only one scale in dictionary (?why, to avoid confusion)

## think about melody as sentence (?bar) (elaborate what can it mean)

## playover selection
### pattern using multi-notes (chord-like) - done, but in case of chords, maybe would be good first\
to reverse chord into scale steps, find closest chord. \
Could be it is done already like that by notes approximation fails here somehow.\
Think about notes approximation (tricky)

~~### work on accents withing pattern (amplitude) -~~ done (fixed for 4/4)
### playing one pattern with different transformation (there are some functions: transpose and invertion + combination of these 2 - possibly known from chords), but also slowdown, spedup, gate (legato/staccato)
### when using appregios please confider different for of chords (based on 4th interval, or second intervals)  - https://www.thejazzpianosite.com/jazz-piano-lessons/jazz-chord-voicings/chord-voicing-rules/ 
* Consonant Voicing – Tertian Harmony (stacked 3rds)
* Dissonant Voicing – Secundal Harmony (Tone Clusters)
* Quartal Harmony

~~### New scales at isobar~~
 ~~draft done, please add to yaml~~ Done
### using existing patterns but only 4/6/10 (not other numbers)
### using existing patterns but with increment more than 1
### using existing pattern but following chord progression (this looks more like appregios, but not necesarily follow appregios
~~### think about using different notes leghts ~~ - done
### this about using rests (pauza) withing lenghts pattern (this in theory allows to use different lengts. e.g. pattern 1-2 can be 1-rest-2 )
### implement/apply inrements with zero diference (unison) - use maybe diminuntions or existing patterns with 0-increment
### chords 
### CC (expression) - dimununtion (special events can go with notes)

## get midi keyboard for selecting notes

# patterns classification
## Detection and Classificationof pattern according too accents in phrase - partially done


# sync to external clock/music
# recognition of live beats, chords progressions with some 'eventual' special python lib to detect
 
